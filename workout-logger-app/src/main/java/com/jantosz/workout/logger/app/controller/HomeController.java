package com.jantosz.workout.logger.app.controller;

import java.util.Map;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {

	@RequestMapping({"/home"})
	@ResponseBody
    @PreAuthorize("hasRole('user')")
	public String home(Map<String, Object> model){
		System.out.println("HomeController: Passing through ...\n\n\n\n");
        //return "home";
		//ModelAndView model = new ModelAndView("home");
		model.put("message", "Hello World in Spring MVC !!!");
		return "home";
	}


}
