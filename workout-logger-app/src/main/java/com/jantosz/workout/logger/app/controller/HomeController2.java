package com.jantosz.workout.logger.app.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
public class HomeController2 {

	@RequestMapping({"/home2"})
	@ResponseBody
    @PreAuthorize("hasRole('admin')")
	public String home(Map<String, Object> model){
		System.out.println("HomeController2: Passing through ...\n\n\n\n");
        //return "home";
		//ModelAndView model = new ModelAndView("home");
		model.put("message", "Hello World in Spring MVC !!!");
		return "home 2";
	}


}
