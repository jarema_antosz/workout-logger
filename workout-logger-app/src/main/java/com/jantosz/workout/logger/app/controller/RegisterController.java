package com.jantosz.workout.logger.app.controller;

import com.jantosz.workout.logger.app.controller.validator.RegisterFormDataValidator;
import com.jantosz.workout.logger.model.User;
import com.jantosz.workout.logger.model.ext.form.RegisterFormData;
import com.jantosz.workout.logger.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Jarek on 22.06.14.
 */
@Controller
public class RegisterController {

    public static final String REGISTER_PAGE_JSP = "register_page";

    private IUserService userService;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    @Autowired
    @Qualifier("bCryptPasswordEncoder")
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView loadForm() {

        ModelAndView modelAndView = new ModelAndView(REGISTER_PAGE_JSP, "registerFormData", new RegisterFormData());
        return modelAndView;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView handleFormData(@ModelAttribute RegisterFormData registerFormData, BindingResult result) {

        RegisterFormDataValidator userValidator = new RegisterFormDataValidator();
        userValidator.validate(registerFormData, result);

        if (result.hasErrors()) {
            return new ModelAndView(REGISTER_PAGE_JSP, result.getModel());
        } else {
            User user = userService.loadUserByLogin(registerFormData.getLogin());

            if (user != null) {
                result.rejectValue("login", "login already exists", "login already exists");
                return new ModelAndView(REGISTER_PAGE_JSP, result.getModel());
            } else {
                createUser(registerFormData);
            }
        }


        return new ModelAndView("redirect:/login");
    }

    private void createUser(RegisterFormData registerFormData) {
        String passwordHash = passwordEncoder.encode(registerFormData.getPassword());
        userService.createUser(registerFormData.getLogin(), passwordHash);
    }


}
