package com.jantosz.workout.logger.app.controller.rest;

import com.jantosz.workout.logger.service.auth.IAuthenticationFacade;
import com.jantosz.workout.logger.service.impl.AuthenticationFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by Jarek on 10.04.14.
 */

@Controller
@RequestMapping({"/rpc"})
public class UserController {

    private IAuthenticationFacade authenticationFacade;

    @Autowired
    public void setAuthenticationFacade(IAuthenticationFacade authenticationFacade) {
        this.authenticationFacade = authenticationFacade;
    }

    @RequestMapping({"/getUserRoles"})
    @ResponseBody
    @PreAuthorize("hasRole('user')")
    public Roles getUserRoles() {

        Authentication auth = authenticationFacade.getAuthentication();
        Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();
        Roles roles = new Roles(authorities);
        return roles;
    }

    private class Roles implements Serializable{

        private List<String> roles;

        public Roles(){
            roles = new ArrayList<String>();
        }

        public Roles(Collection<? extends GrantedAuthority> authorities){
            roles = new ArrayList<String>();
            for(GrantedAuthority authority: authorities){
                roles.add(authority.getAuthority());
            }
        }

        public List<String> getRoles() {
            return roles;
        }

        public void setRoles(List<String> roles) {
            this.roles = roles;
        }
    }
}
