package com.jantosz.workout.logger.app.controller.validator;

import com.jantosz.workout.logger.model.ext.form.RegisterFormData;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by Jarek on 22.06.14.
 */
public class RegisterFormDataValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return RegisterFormData.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        RegisterFormData user = (RegisterFormData) target;

        if (user.getLogin() == null || "".equals(user.getLogin())) {
            errors.rejectValue("login", "cannot be empty", "cannot be empty");
        }

        if (user.getPassword() == null || "".equals(user.getPassword())) {
            errors.rejectValue("password", "cannot be empty", "cannot be empty");
        }

        if (user.getRetypedPassword() == null || "".equals(user.getRetypedPassword())) {
            errors.rejectValue("retypedPassword", "cannot be empty", "cannot be empty");
        }

        if (user.getPassword() != null && user.getRetypedPassword() != null) {
            if (!user.getPassword().equals(user.getRetypedPassword())) {
                errors.rejectValue("password", "different password values", "different password values");
                errors.rejectValue("retypedPassword", "different password values", "different password values");
            }
        }

    }
}
