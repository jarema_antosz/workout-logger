<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="resources/css/vendor/bootstrap.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Custom Login Page</title>
</head>
<body onload='document.loginForm.j_username.focus();'>
<div class="container">
    <h3>Custom Login Page</h3>

    <%

        String errorString = (String) request.getAttribute("error");
        if (errorString != null && errorString.trim().equals("true")) {
            out.println("<div class=\"alert alert-danger\" role=\"alert\">Incorrect login name or password. Please retry using correct login name and password.</div>");
        }
    %>

    <form name='loginForm' role="form" action="<c:url value='j_spring_security_check' />" method='POST'>
        <div class="form-group">
            <label for="j_username">User:</label>
            <input type="text" name="j_username" class="form-control" id="j_username" placeholder="Enter login">
        </div>
        <div class="form-group">
            <label for="j_password">Password:</label>
            <input type="password" name="j_password" class="form-control" id="j_password" placeholder="Enter password">
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
        <a href="./register" role="button" class="btn btn-success btn-large">Register account</a>
    </form>




</div>
</body>
</html>