<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="resources/css/vendor/bootstrap.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Register User</title></head>
<body>
<div class="container">
    <form:form role="form" action="register" method="post" commandName="registerFormData">

        <div class="form-group">
            <label for="login">Login:</label>
            <input type="text" name="login" class="form-control" id="login" placeholder="Enter login">
            <form:errors path="login" cssClass="alert alert-danger" element="div" />
        </div>
        <div class="form-group">
            <label for="password">Password:</label>
            <input type="text" name="password" class="form-control" id="password" placeholder="Enter password">
            <form:errors path="password" cssClass="alert alert-danger" element="div" />
        </div>
        <div class="form-group">
            <label for="retypedPassword">Retyped password:</label>
            <input type="text" name="retypedPassword" class="form-control" id="retypedPassword"
                   placeholder="Retype password">
            <form:errors path="retypedPassword" cssClass="alert alert-danger" element="div" />
        </div>
        <button type="submit" class="btn btn-default">Submit</button>


    </form:form>
</div>
</body>
</html>