var workoutLoggerModule = angular.module("workoutLogger",['ngRoute']);

workoutLoggerModule.config(function($httpProvider){
    $httpProvider.responseInterceptors.push('retryInterceptor');
})
    .factory('retryInterceptor', function ($injector, $q) {
        return function(responsePromise) {
            return responsePromise.then(null, function(errResponse) {
                if (errResponse.status === 503) {
                    return $injector.get('$http')(errResponse.config);
                } else {
                    return $q.reject(errResponse);
                }
            });
        };
    });
workoutLoggerModule.controller('mainController',
    function($scope, $http, $location, $element) {

        $scope.isAdmin = false;

        $scope.changeClass = function() {
            if ($scope.class == "active") {

                $scope.class = "";
                console.log("clear active");
            }
            else {
                $scope.class = "active";
                console.log("set active");
            }
        };

        $scope.getUserRoles = function(){
            $http.get("rpc/getUserRoles").success(function(data){
                console.log("data: " + data);
                $scope.roles = data.roles;


                for(var role in $scope.roles){
                    if($scope.roles[role] == "admin"){
                        $scope.isAdmin = true;
                        break;
                    }
                }

            });
        }

        $scope.isActive = function (viewLocation) {
            return viewLocation === $location.path();
        };


        $scope.getUserRoles();

        $location.path("/home");


    }

);



workoutLoggerModule.controller('ContactController',
    function($scope, $http){});
workoutLoggerModule.controller('ManageController',
    function($scope, $http){});
workoutLoggerModule.controller('AboutController',
    function($scope, $http){});

workoutLoggerModule.config(function($routeProvider){
    $routeProvider.when("/",
        {
            controller:"HomeController",
            templateUrl:"resources/html/templates/views/home.html"
        })
        .when("/home",
        {
            controller:"HomeController",
            templateUrl:"resources/html/templates/views/home.html"
        })
        .when("/contact",
        {
            controller:"ContactController",
            templateUrl:"resources/html/templates/views/contact.html"
        })
        .when("/manage",
        {
            controller:"ManageController",
            templateUrl:"resources/html/templates/views/manage.html"
        })
        .when("/about",
        {
            controller:"AboutController",
            templateUrl:"resources/html/templates/views/about.html"
        })
        .otherwise({redirectTo:"/"});
});