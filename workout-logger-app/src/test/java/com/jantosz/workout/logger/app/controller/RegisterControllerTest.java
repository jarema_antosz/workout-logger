package com.jantosz.workout.logger.app.controller;

import com.jantosz.workout.logger.model.User;
import com.jantosz.workout.logger.model.ext.form.RegisterFormData;
import com.jantosz.workout.logger.service.IUserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by Jarek on 22.06.14.
 */

@RunWith(MockitoJUnitRunner.class)
public class RegisterControllerTest {

    private static final int REDIRECT_STATUS = 302;
    private static final String PASSWORD = "AAA";
    private MockMvc mockMvc;

    @Mock
    private IUserService userService;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        RegisterController registerController = new RegisterController();
        registerController.setUserService(userService);
        registerController.setPasswordEncoder(passwordEncoder);

        mockMvc = MockMvcBuilders.standaloneSetup(registerController).build();
    }

    private ResultActions performGetRequest() throws Exception {
        return mockMvc.perform(get("/register"));
    }

    private ResultActions performPostRequest(RegisterFormData formData) throws Exception {
        return mockMvc.perform(post("/register", formData)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("login", formData.getLogin())
                .param("password", formData.getPassword())
                .param("retypedPassword", formData.getRetypedPassword()));
    }

    @Test
    public void canAccessUrl() throws Exception {

        ResultActions result = performGetRequest();

        result.andExpect(status().isOk());
    }

    @Test
    public void registerUrlReturnsForm() throws Exception {

        ResultActions result = performGetRequest();

        result.andExpect(view().name(RegisterController.REGISTER_PAGE_JSP));
    }

    @Test
    public void canPerformPostRequest() throws Exception {
        ResultActions result = performPostRequest(new RegisterFormData());
        result.andExpect(status().isOk());
    }

    @Test
    public void post_shouldValidateNullLogin() throws Exception {
        RegisterFormData formData = new RegisterFormData();
        ResultActions result = performPostRequest(formData);
        result.andExpect(status().isOk()).andExpect(model().attributeHasFieldErrors("registerFormData", "login"));
    }

    @Test
    public void post_shouldValidateEmptyLogin() throws Exception {
        RegisterFormData formData = new RegisterFormData();
        formData.setLogin("");
        ResultActions result = performPostRequest(formData);
        result.andExpect(status().isOk()).andExpect(model().attributeHasFieldErrors("registerFormData", "login"));
    }

    @Test
    public void post_shouldValidateEmptyPassword() throws Exception {
        RegisterFormData formData = new RegisterFormData();
        formData.setPassword("");
        ResultActions result = performPostRequest(formData);
        result.andExpect(status().isOk()).andExpect(model().attributeHasFieldErrors("registerFormData", "password"));
    }

    @Test
    public void post_shouldValidateEmptyRetypedPassword() throws Exception {
        RegisterFormData formData = new RegisterFormData();
        formData.setPassword("");
        ResultActions result = performPostRequest(formData);
        result.andExpect(status().isOk()).andExpect(model().attributeHasFieldErrors("registerFormData", "retypedPassword"));
    }

    @Test
    public void post_shouldValidateDifferentPasswords_retyped_empty() throws Exception {
        RegisterFormData formData = new RegisterFormData();
        formData.setPassword("AAA");
        formData.setRetypedPassword("");
        ResultActions result = performPostRequest(formData);
        result.andExpect(status().isOk()).
                andExpect(model().attributeHasFieldErrors("registerFormData", "retypedPassword")).
                andExpect(model().attributeHasFieldErrors("registerFormData", "password"));
    }

    @Test
    public void post_shouldValidateDifferentPasswords_password_empty() throws Exception {
        RegisterFormData formData = new RegisterFormData();
        formData.setPassword("BBB");
        formData.setRetypedPassword("AAA");
        ResultActions result = performPostRequest(formData);
        result.andExpect(status().isOk()).
                andExpect(model().attributeHasFieldErrors("registerFormData", "retypedPassword")).
                andExpect(model().attributeHasFieldErrors("registerFormData", "password"));
    }

    @Test
    public void post_showNoErrorsOnValidData() throws Exception {
        RegisterFormData formData = createCorrectRegisterFormData("someUser");
        ResultActions result = performPostRequest(formData);

        result.andExpect(status().is(302)).andExpect(model().errorCount(0));
    }

    @Test
    public void post_shouldCheckIfDatabaseContainsLogin() throws Exception {

        String login = "someUser";

        RegisterFormData formData = createCorrectRegisterFormData(login);
        ResultActions result = performPostRequest(formData);

        when(userService.loadUserByLogin(login)).thenReturn(null);

        verify(userService).loadUserByLogin(login);

    }

    @Test
    public void post_ifDatabaseContainsLogin_then_return_error() throws Exception {

        String login = "someUser";

        RegisterFormData formData = createCorrectRegisterFormData(login);

        when(userService.loadUserByLogin(anyString())).thenReturn(new User());

        ResultActions result = performPostRequest(formData);


        verify(userService).loadUserByLogin(anyString());

        result.andExpect(status().isOk()).andExpect(model().attributeHasFieldErrors("registerFormData", "login"));

    }

    @Test
    public void post_ifDatabaseNotContainsLogin_then_redirect_to_login() throws Exception {

        String login = "someUser";

        RegisterFormData formData = createCorrectRegisterFormData(login);

        when(userService.loadUserByLogin(anyString())).thenReturn(null);

        ResultActions result = performPostRequest(formData);

        result.andExpect(status().is(REDIRECT_STATUS)).andExpect(redirectedUrl("/login"));

    }

    @Test
    public void post_ifCorrectRequest_should_encode_password() throws Exception {

        String login = "someUser";

        RegisterFormData formData = createCorrectRegisterFormData(login);

        when(userService.loadUserByLogin(anyString())).thenReturn(null);

        ResultActions result = performPostRequest(formData);

        verify(passwordEncoder).encode(PASSWORD);

    }


    @Test
    public void post_ifCorrectRequest_should_create_user() throws Exception {

        String login = "someUser";

        RegisterFormData formData = createCorrectRegisterFormData(login);

        when(userService.loadUserByLogin(anyString())).thenReturn(null);

        performPostRequest(formData);

        verify(userService).createUser(anyString(), anyString());

    }

    @Test
    public void post_ifCorrectRequest_should_pass_encoded_password_when_creating_user() throws Exception {

        String login = "someUser";

        RegisterFormData formData = createCorrectRegisterFormData(login);

        when(userService.loadUserByLogin(anyString())).thenReturn(null);
        String dummyHash = "XXXXX";
        when(passwordEncoder.encode(PASSWORD)).thenReturn(dummyHash);

        performPostRequest(formData);

        ArgumentCaptor<String> argumentLogin = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> argumentPassword = ArgumentCaptor.forClass(String.class);


        verify(userService).createUser(argumentLogin.capture(), argumentPassword.capture());

        assertEquals("should use passed login", login, argumentLogin.getValue());
        assertEquals("should use created password hash", dummyHash, argumentPassword.getValue());

    }

    private RegisterFormData createCorrectRegisterFormData(String login) {
        RegisterFormData formData = new RegisterFormData();
        formData.setLogin(login);
        formData.setPassword(PASSWORD);
        formData.setRetypedPassword(PASSWORD);
        return formData;
    }
}
