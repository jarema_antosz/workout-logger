package com.jantosz.workout.logger.app.controller.rest;

import com.jantosz.workout.logger.service.auth.IAuthenticationFacade;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Collection;

import static com.jayway.jsonassert.JsonAssert.with;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 * Created by Jarek on 10.04.14.
 */

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

    private MockMvc mockMvc;

    @Mock
    private IAuthenticationFacade authenticationFacade;

    @Before
    public void setUp(){
        UserController userController = new UserController();

        Authentication auth = getAuthentication();
        when(authenticationFacade.getAuthentication()).thenReturn(auth);

        userController.setAuthenticationFacade(authenticationFacade);
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    public void canAccess_getUserRoles() throws Exception{
        Authentication auth = getAuthentication();

        when(authenticationFacade.getAuthentication()).thenReturn(auth);
        performRequest().andExpect(status().isOk());
    }

    private ResultActions performRequest() throws Exception {
        return mockMvc.perform(get("/rpc/getUserRoles"));
    }

    @Test
    public void getUserRoles_shouldGetUserFromSecurityContext() throws Exception{

        performRequest();

        Mockito.verify(authenticationFacade).getAuthentication();
    }

    @Test
    public void getUserRoles_shouldReturnJSON() throws Exception{

        ResultActions result = performRequest();

        result.andExpect(content().contentType("application/json;charset=UTF-8"));
        result.andExpect(jsonPath("roles").exists());
        result.andExpect(jsonPath("roles").isArray());
        result.andExpect(jsonPath("roles", hasSize(2)));
        result.andExpect(jsonPath("roles", hasItems("user", "admin")));

        Mockito.verify(authenticationFacade).getAuthentication();
    }

    private Authentication getAuthentication() {
        return new Authentication() {
                @Override
                public Collection<? extends GrantedAuthority> getAuthorities() {
                    GrantedAuthority user = new SimpleGrantedAuthority("user");
                    GrantedAuthority admin = new SimpleGrantedAuthority("admin");
                    Collection<GrantedAuthority> auths = new ArrayList<GrantedAuthority>();
                    auths.add(user);
                    auths.add(admin);
                    return auths;
                }

                @Override
                public Object getCredentials() {
                    return null;
                }

                @Override
                public Object getDetails() {
                    return null;
                }

                @Override
                public Object getPrincipal() {
                    return null;
                }

                @Override
                public boolean isAuthenticated() {
                    return false;
                }

                @Override
                public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

                }

                @Override
                public String getName() {
                    return null;
                }
            };
    }
}
