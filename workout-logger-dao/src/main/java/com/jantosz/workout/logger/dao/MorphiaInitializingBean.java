package com.jantosz.workout.logger.dao;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;


@Component
public class MorphiaInitializingBean implements InitializingBean {
	
	@Autowired
	private Morphia morphia;
	
	@Autowired
	private Datastore ds;

	@Override
	public void afterPropertiesSet() throws Exception {
		morphia.mapPackage("com.jantosz.workout.logger.model");
		ds.ensureIndexes();
		
	}

}
