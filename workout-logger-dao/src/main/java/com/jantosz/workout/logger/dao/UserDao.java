package com.jantosz.workout.logger.dao;


import com.jantosz.workout.logger.model.Workout;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jantosz.workout.logger.model.User;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class UserDao extends BasicDAO<User, String> {

	@Autowired
	public UserDao(Datastore ds) {
		super(User.class, ds);
		
	}

    @Autowired
    private WorkoutDao workoutDao;


    public User findByLogin(String login) {

        Query<User> query = getDatastore().createQuery(User.class);
        User user = query.field("login").equal(login).get();
        return user;
    }

//    @PostConstruct
//    public void postConstruct(){
//        User user = new User();
//        user.setLogin("BBBB");
//        user.setPassword("BBBB");
//
//        Workout w = new Workout();
//        w.setStartDate(new Date());
//        w.setEndDate(new Date());
//        w.setDistance(500L);
//
//        workoutDao.save(w);
//
//        List<Workout> lastWorkouts = new ArrayList<Workout>();
//        lastWorkouts.add(w);
//        user.setLastWorkouts(lastWorkouts);
//
//        save(user);
//    }
}
