package com.jantosz.workout.logger.dao;

import com.jantosz.workout.logger.model.User;
import com.jantosz.workout.logger.model.Workout;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Jarek on 14.06.14.
 */
@Repository
public class WorkoutDao extends BasicDAO<Workout, String> {

    @Autowired
    public WorkoutDao(Datastore ds) {
        super(Workout.class, ds);

    }
}
