package com.jantosz.workout.logger.dao;

import java.net.UnknownHostException;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.jantosz.workout.logger.model.User;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.WriteConcern;



public class SomeTest {
	
	@Test
	@Ignore
	public void doTest() throws UnknownHostException{
		Mongo mongo = new MongoClient();
		Morphia morphia = new Morphia();
		morphia.map(User.class);
		
		Datastore ds = morphia.createDatastore(mongo, "test");
		
		User user = new User();
		user.setLogin("test12345");
		ds.save(user, WriteConcern.JOURNALED);
		
		//user = ds.find(User.class,"login=","test1234").get();
		
		//ds.delete(user);
	}

}
