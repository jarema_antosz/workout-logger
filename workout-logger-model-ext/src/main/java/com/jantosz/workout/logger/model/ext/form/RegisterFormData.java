package com.jantosz.workout.logger.model.ext.form;

import java.io.Serializable;

/**
 * Created by Jarek on 22.06.14.
 */
public class RegisterFormData implements Serializable {

    private String login;
    private String password;
    private String retypedPassword;

    public RegisterFormData(){

    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRetypedPassword() {
        return retypedPassword;
    }

    public void setRetypedPassword(String retypedPassword) {
        this.retypedPassword = retypedPassword;
    }
}
