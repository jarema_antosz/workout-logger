package com.jantosz.workout.logger.model;

/**
 * Created by Jarek on 25.06.14.
 */
public enum Roles {
    USER {
        public String toString() {
            return "user";
        }
    },

    ADMIN {
        public String toString() {
            return "admin";
        }
    }
}
