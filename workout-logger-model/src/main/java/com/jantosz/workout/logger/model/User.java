package com.jantosz.workout.logger.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;




@Entity("users")
public class User {

	@Id
	private String login;
	private String password;
	@Indexed
	private Date createDate;
    private List<String> roles = new ArrayList<String>();
    private List<Workout> lastWorkouts;
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public List<Workout> getLastWorkouts() {
        return lastWorkouts;
    }

    public void setLastWorkouts(List<Workout> lastWorkouts) {
        this.lastWorkouts = lastWorkouts;
    }

    @Override
	public String toString() {
		return "User [login=" + login + ", password=" + password
				+ ", createDate=" + createDate + "]";
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (createDate != null ? !createDate.equals(user.createDate) : user.createDate != null) return false;
        if (!login.equals(user.login)) return false;

        return true;
    }

    public User() {
    }

    @Override
    public int hashCode() {
        int result = login.hashCode();
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        return result;
    }
}
