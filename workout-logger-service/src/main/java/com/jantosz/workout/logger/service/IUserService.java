package com.jantosz.workout.logger.service;

import com.jantosz.workout.logger.model.User;

import java.util.List;

/**
 * Created by Jarek on 12.03.14.
 */
public interface IUserService {
    List<User> loadAll();

    User loadUserByLogin(String login);

    void createUser(String login, String password);
}
