package com.jantosz.workout.logger.service.auth;

import com.jantosz.workout.logger.service.IUserService;
import com.jantosz.workout.logger.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Created by Jarek on 11.03.14.
 */

@Service("myUserDetailsService")
public class MyUserDetailsService implements UserDetailsService {

    private IUserService userService;

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        com.jantosz.workout.logger.model.User user = userService.loadUserByLogin(username);

        if (user == null) {
            throw new UsernameNotFoundException(username);
        }

        Collection<? extends GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(getRoles(user));

        if(authorities.size() == 0){
            throw new UsernameNotFoundException(username);
        }
        return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(), authorities);
    }

    private String[] getRoles(com.jantosz.workout.logger.model.User user) {

        String [] roles = {};
        if(user.getRoles() == null || user.getRoles().size() == 0){
            return roles;
        }
        roles = user.getRoles().toArray(new String[user.getRoles().size()]);
        return roles;
    }
}
