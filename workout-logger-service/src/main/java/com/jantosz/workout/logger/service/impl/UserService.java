package com.jantosz.workout.logger.service.impl;

import com.jantosz.workout.logger.dao.UserDao;
import com.jantosz.workout.logger.model.Roles;
import com.jantosz.workout.logger.model.User;
import com.jantosz.workout.logger.service.IUserService;
import org.mongodb.morphia.query.QueryResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements IUserService {


    private UserDao userDao;

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public List<User> loadAll() {
        QueryResults<User> result = userDao.find();

        return result.asList();
    }

    @Override
    public User loadUserByLogin(String login) {
        return userDao.findByLogin(login.toLowerCase());
    }

    @Override
    public void createUser(String login, String password) {

        User user = new User();
        user.setLogin(login.toLowerCase());
        user.setPassword(password);
        user.getRoles().add(Roles.USER.toString());
        userDao.save(user);
    }

}
