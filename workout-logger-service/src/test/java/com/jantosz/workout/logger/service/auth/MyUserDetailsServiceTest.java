package com.jantosz.workout.logger.service.auth;

import com.jantosz.workout.logger.model.User;
import com.jantosz.workout.logger.service.IUserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

/**
 * Created by Jarek on 16.03.14.
 */
@RunWith(MockitoJUnitRunner.class)
public class MyUserDetailsServiceTest {

    private MyUserDetailsService classUnderTest;

    @Mock
    private IUserService userService;

    @Before
    public void setUp(){
        classUnderTest = new MyUserDetailsService();
        classUnderTest.setUserService(userService);

    }

    private User createUser(String username){
        User sampleUSer = new User();
        sampleUSer.setLogin(username);
        sampleUSer.setPassword("123456");
        return sampleUSer;
    }

    @Test
    public void loadUserByUsername_shouldCall_UserService(){

        String username = "some_user";
        User sampleUSer = createUser(username);
        List<String> roles = new ArrayList<String>();
        roles.add("role1");
        sampleUSer.setRoles(roles);

        when(userService.loadUserByLogin(username)).thenReturn(sampleUSer);

        classUnderTest.loadUserByUsername(username);

        Mockito.verify(userService, Mockito.times(1)).loadUserByLogin(username);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void loadUserByUsername_shouldThrow_UsernameNotFoundException_when_no_granted_authorities(){

        String username = "some_user";
        User sampleUSer = createUser(username);


        when(userService.loadUserByLogin(username)).thenReturn(sampleUSer);

        classUnderTest.loadUserByUsername(username);

        Mockito.verify(userService, Mockito.times(1)).loadUserByLogin(username);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void loadUserByUsername_shouldThrow_UsernameNotFoundException_when_user_not_found(){

        String username = "some_user";

        when(userService.loadUserByLogin(username)).thenReturn(null);

        classUnderTest.loadUserByUsername(username);

    }
}
