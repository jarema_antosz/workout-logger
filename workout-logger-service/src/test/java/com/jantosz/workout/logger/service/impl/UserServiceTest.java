package com.jantosz.workout.logger.service.impl;

import com.jantosz.workout.logger.dao.UserDao;
import com.jantosz.workout.logger.model.Roles;
import com.jantosz.workout.logger.model.User;
import com.jantosz.workout.logger.service.IUserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Jarek on 12.03.14.
 */

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    private IUserService classUnderTest;

    @Mock
    private UserDao userDao;

    @Before
    public void setUp() {
        classUnderTest = new UserService();
        UserService userService = (UserService) classUnderTest;
        userService.setUserDao(userDao);

    }

    @Test
    public void canLoadUserByLogin() {
        String login = "test";
        User user = classUnderTest.loadUserByLogin(login);
    }

    @Test
    public void loadUserByLoginShouldCallUserDao() {

        String login = "test";
        User sampleUser = new User();
        sampleUser.setLogin(login);
        when(userDao.findByLogin(login)).thenReturn(sampleUser);

        User user = classUnderTest.loadUserByLogin(login);

        Mockito.verify(userDao, Mockito.times(1)).findByLogin(login);
    }

    @Test
    public void loadUserByLoginShouldReturnUserFromDao() {

        String login = "test";
        User sampleUser = new User();
        sampleUser.setLogin(login);
        when(userDao.findByLogin(login)).thenReturn(sampleUser);

        User user = classUnderTest.loadUserByLogin(login);

        assertTrue("Users should be equal", sampleUser.equals(user));
    }

    @Test
    public void createUserShouldCallUserDao(){

        String login = "A";
        String password = "B";
        classUnderTest.createUser(login, password);

        verify(userDao).save(any(User.class));

    }

    @Test
     public void createUserShouldSetValuesOnUserBeforeSave(){

        String login = "A";
        String password = "B";

        ArgumentCaptor<User> argument = ArgumentCaptor.forClass(User.class);

        classUnderTest.createUser(login, password);

        verify(userDao).save(argument.capture());

        assertEquals("should use passed login", login.toLowerCase(), argument.getValue().getLogin());
        assertEquals("should use passed password", password, argument.getValue().getPassword());

    }

    @Test
    public void createUserShouldSetRoleUser(){

        String login = "A";
        String password = "B";

        ArgumentCaptor<User> argument = ArgumentCaptor.forClass(User.class);

        classUnderTest.createUser(login, password);

        verify(userDao).save(argument.capture());

        assertTrue("should have default role user", argument.getValue().getRoles().contains(Roles.USER.toString()));

    }
}
